import sqlite3

# creating database
conn = sqlite3.connect('cars_db.db')

conn.execute('''CREATE TABLE cars_db
         (ID           INTEGER     PRIMARY KEY NOT NULL,
          NAME        TEXT    NOT NULL,
          TYPE        TEXT    NOT NULL,
          COST       REAL);''')

conn.commit()
conn.close()



