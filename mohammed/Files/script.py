import os
import runpy

# define the name of the directory to be created
path = "Root"

try:
    os.makedirs(path)
except OSError:
    print ("Creation of the directory %s failed" % path)
else:
    print ("Successfully created the directory %s" % path)

i = 1
while i <= 10:
    parent_path = ('C:/Users/moham/Downloads/_3_Python_oops/_3_Python_oops/assignment/Root')
    path = str(parent_path)
    os.chdir(parent_path)
    filename = (f'python{str(i)}')
    os.mkdir(filename)
    new_path = path + '/' + filename
    os.chdir(new_path)
    os.getcwd()
    file = open(f'python{str(i)}.py', 'w')
    file.write(f"print(f'Hello python {str(i)} !!')")
    file = open(f'python{str(i)}.py', 'rb')
    runpy.run_path(str('C:/Users/moham/Downloads/_3_Python_oops/_3_Python_oops/assignment/Root/python'+str(i)+'/python'+str(i)+'.py'))
    i += 1