from tkinter import *
import tkinter.messagebox
from PIL import ImageTk,Image
from Car_db import car_data
import csv,sqlite3

class Car:

    def __init__(self,window):
        self.window = window
        self.window.title("Purchase Car")
        self.window.geometry("400x600+300+200")
        self.window.config(background="grey")
        # self.canvas=Canvas(window,width=400,heigth=600)
        # self.image=ImageTk.PhotoImage(Image.open("D:\PYTHON_workspace\demo\\images.jpg"))
        # self.canvas.create_image(self,0,anchor=NW,image=image).pack()

        Cname = StringVar()
        Cbrand = StringVar()
        Ctype = StringVar()
        Ccost = StringVar()

        def clear():
            clear = tkinter.messagebox.askokcancel("Car purchase", "Confirm if you want to clear?")
            if clear>0:
                Cname.set("")
                Cbrand.set("--select car brand--")
                Ctype.set("")
                Ccost.set("")
                return

        def submit():
            submit = tkinter.messagebox.askyesno("Car purchase","Confirm if you want to submit?")
            if submit>0:
                item1= Cname.get()
                item2= Cbrand.get()
                item3= Ctype.get()
                item4= Ccost.get()
                param = {
                    'NAME': item1,
                    'BRAND': item2,
                    'TYPE': item3,
                    'COST': item4
                }
                print(item1, item2, item3, item4)
                car_data(param)

                return

        def report():
            with open('report.csv', mode='w+') as csv_file:
                writer = csv.writer(csv_file, delimiter=',')
                conn = sqlite3.connect('Car_db.db')
                query2 = ("SELECT * FROM CAR")
                details = conn.execute(query2)
                writer.writerow(['CAR_NAME', 'CAR_BRAND', 'CAR_TYPE', 'CAR_COST'])

                line_count = 0
                for row in details:
                    if line_count == 0:
                        writer.writerow([row[0], row[1], row[2], row[3]])
                        line_count += 1
                    else:
                        writer.writerow([row[0], row[1], row[2], row[3]])
                        line_count += 1

                conn.commit()
                conn.close()

        def show():
            display1 = Label(window, text=("The car name is {}".format(Cname.get())), fg="blue", bg="grey", font=("aerial", 10)).place(x=30,y=350)
            display2 = Label(window, text=("The car brand is {}".format(Cbrand.get())), fg="blue", bg="grey", font=("aerial", 10)).place(x=30,y=370)
            display3= Label(window, text=("The car type is {}".format(Ctype.get())), fg="blue", bg="grey", font=("aerial", 10)).place(x=30,y=390)
            display4= Label(window, text=("The car cost is Rs.{}".format(Ccost.get())), fg="blue", bg="grey", font=("aerial", 10)).place(x=30,y=410)

        self.label1 = Label(window, text="Car Name", fg="blue", bg="grey", font=("aerial", 12, "bold")).place(x=20,
                                                                                                              y=30)
        self.entry1 = Entry(window, textvar=Cname).place(x=120, y=30, width=130, height=30)

        self.label2 = Label(window, text="Car Brand", fg="blue", bg="grey", font=("aerial", 12, "bold")).place(x=20,y=90)
        v = ["BMW", "Toyota", "Renault", "AUDI", "Jaguar", "Suzuki"]
        self.entry2 = OptionMenu(window, Cbrand, *v).place(x=120, y=90, width= 130)
        Cbrand.set("--select car--")

        self.label3 = Label(window, text="Car Type", fg="blue", bg="grey", font=("aerial", 12, "bold")).place(x=20,y=150)
        self.entry3 = Entry(window, textvar=Ctype).place(x=120, y=150, width=130, height=30)

        self.label4 = Label(window, text="Car Cost", fg="blue", bg="grey", font=("aerial", 12, "bold")).place(x=20,y=210)
        self.entry4 = Entry(window, textvar=Ccost).place(x=120, y=210, width=130, height=30)

        self.button1 = Button(window, text="Submit", fg="black", bg="white",command=submit,
                              font=("aerial", 12, "bold"))
        self.button1.place(x=40, y=280, width=90)
        self.button2 = Button(window, text="Show", fg="black", bg="white",command=show,
                              font=("aerial", 12, "bold"))
        self.button2.place(x=160, y=280,width=90)
        self.button3 = Button(window, text="Clear", fg="black", bg="white",command=clear,
                              font=("aerial", 12, "bold"))
        self.button3.place(x=280, y=280, width=90)

        self.button4 = Button(window, text="Generate Report", fg="black", bg="cyan", command=report,
                              font=("aerial", 12, "bold"))
        self.button4.place(x=130, y=340, width=150)

if __name__=='__main__':
    window= Tk()
    app=Car(window)
    window.mainloop()


