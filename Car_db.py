import sqlite3


def car_data(data):
    conn= sqlite3.connect('Car_db.db')
    # conn.execute('''CREATE TABLE CAR
    #                (CAR_NAME TEXT NOT NULL,
    #                CAR_BRAND TEXT,
    #                CAR_TYPE TEXT,
    #                CAR_COST INT)''')

    query=('INSERT  OR REPLACE INTO CAR (CAR_NAME, CAR_BRAND, CAR_TYPE, CAR_COST) '
           'VALUES(:NAME, :BRAND, :TYPE, :COST)')

    conn.execute(query, data)
    conn.commit()
    conn.close()