from tkinter import *
from insert_db import store_data
import tkinter.messagebox

class carsGUI:

    def __init__(self,master):
        self.CarID = IntVar()
        self.CarName= StringVar()
        self.CarType= StringVar()
        self.CarCost= IntVar()


        self.label1 = Label(master,font=("ariel",20,'bold'),justify=LEFT,text='Car Id:',padx=2,pady=3,bg="Ghost white")
        self.label1.grid(row=5,column=0)

        self.entry= Entry(master, font=("ariel",20,'bold'),text='', textvariable=self.CarID,width=25)
        self.entry.grid(row=5, column=1)

        self.label1=Label(master,font=("ariel",20,'bold'),justify=LEFT,text='Car Name:',padx=2,pady=3,bg="Ghost White")
        self.label1.grid(row=6,column=0)

        self.entry1=Entry(master,font=("ariel",20,'bold'),text='',textvariable=self.CarName,width=25)
        self.entry1.grid(row=6,column=1)

        self.label2 = Label(master,font=("ariel",20,'bold'), justify=LEFT,text='Car Type:',padx=2,pady=3,bg="Ghost White")
        self.label2.grid(row=7, column=0)

        self.entry2 = Entry(master, font=("ariel",20,'bold'),text='', textvariable=self.CarType,width=25)
        self.entry2.grid(row=7, column=1)

        self.label3 = Label(master,font=("ariel",20,'bold'),justify=LEFT,text='Car Cost :',padx=2,pady=3,bg="Ghost White")
        self.label3.grid(row=8, column=0)

        self.entry3 = Entry(master, font=("ariel",20,'bold'),text='', textvariable=self.CarCost,width=25)
        self.entry3.grid(row=8,column=1)

        self.btn1=Button(master,font=("ariel",20,'bold'),height=1,width=7, text='Submit',command=self.submit)
        self.btn1.grid(row=50,column=0)

        self.btn2=Button(master,font=("ariel",20,'bold'),height=1,width=7,text='Show',command=self.show)
        self.btn2.grid(row=50,column=1)

        self.btn3=Button(master,font=("ariel",20,'bold'),height=1,width=7,text='Clear',command=self.clear)
        self.btn3.grid(row=51,column=0)

        self.btn4=Button(master,font=("ariel",20,'bold'),height=1,width=7,text='Exit',command=self.iexit)
        self.btn4.grid(row=51,column=1)

        self.lbl=StringVar()
        self.label4=Label(master,textvariable=self.lbl,font=("ariel",20,'bold'))
        self.label4.grid(row=50,column=2)

    def submit(self):
        self.id = self.CarID.get()
        self.name=self.CarName.get()
        self.type=self.CarType.get()
        self.cost=self.CarCost.get()


        param={
            'ID':self.id,
            'NAME':self.name,
            'TYPE':self.type,
            'COST':self.cost
            }
        print(self.id,self.name,self.type,self.cost)
        store_data(param)

    def show(self):
        self.text= self.id,self.name,self.type,self.cost
        self.lbl.set(self.text)
        return self.lbl


    def clear(self):
        self.entry.delete(0, END)
        self.entry1.delete(0,END)
        self.entry2.delete(0,END)
        self.entry3.delete(0,END)

    def iexit(self):
        self.iexit = tkinter.messagebox.askyesno("Car Database System","Confirm if you want to exit")
        if self.iexit > 0:
         root.destroy()
         return()


root=Tk()
root.geometry('1000x500')
app=carsGUI(root)
root.title("Car Database System")
root.mainloop()